


## Open Source Contributions

* **Waves:** a Ruby web application framework. I designed and developed new features, wrote the bulk of the tests and documentation, and managed releases while the project was funded by AT&T interactive.
**Filebase:** a file-based datastore for Ruby.  I reworked the project to support multiple storage formats (JSON, YAML, Marshal) and added indexing and foreign key features.
**AutoCode:** an attempt at magical source loading (and reloading!) in Ruby. Used in Waves. AutoCode makes heavy use of metaprogramming, and my initial contribution was to refactor the code so that it was readable by mortals.
**Functor:** added pattern-based method dispatch to Ruby.  Even more magical than AutoCode.  I took the initial idea and code, then ran like the wind.
* **Pez:** an [old Forth](https://www.fourmilab.ch/atlast/) made new.  I contributed to the REPL, rewrote the lexer, refactored some troublesome code, and wrote tests.
* **BiteScript & Ruby2Java:** JRuby sideprojects.  Fixed bugs, reworked specs, implemented a minor feature


## Major Projects for spire.io

### **Shark:** distributed backend for spire.io's HTTP ReST API  

Shark was designed with flexibility and scalability in mind. It consists of HTTP dispatchers, a messaging system, and task-processing workers.  The dispatchers perform content-negotiation


**Technologies:** Node.js, CoffeeScript, Redis, Ruby.


### Dis: HTTP ReST backend for the Glass browser extension  
**Technologies:** Ruby, Rack, Sequel, MySQL

### Poorsh: HTTP push notifications for Glass
**Technologies:** Ruby, EventMachine


## Major Projects for AT&T interative

### Gin: Ruby and Java interfaces to AT&T’s FSM library

**Technologies:** Ruby, ruby-ffi, Java, JNA, C, [AT&T FSM library](http://www2.research.att.com/~fsmtools/fsm/)

### Moonstone: a JRuby interface to Lucene
**Technologies:** JRuby, Lucene

### Fenrir: Solr extensions to support Learn to Rank
**Technologies:** Solr, Lucene, Java, JRuby, Ruby2Java


## Interesting Minor Projects

### Synopticon: synchronize DOM/CSS changes between two browsers
* Curatron: research into web content curation
* Squeeze: data reduction
* Cassandra: pure-Ruby CSS generator




