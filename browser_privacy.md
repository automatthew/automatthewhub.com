# Privacy and the Web Browser

This is a report I wrote in spring 2011 to assess the security and privacy implications of browser based applications.  It was written for the executives and potential investors of a company desiring to write browser based applications, so most of the technical details are glossed over, if not fudged.  Some of the statements about specific browsers are probably no longer correct, but the basic line of argument still stands.

---

A promise of privacy in a server-based application means, broadly, that the application vendor will keep a user's information secret from third parties. It may additionally involve the vendor keeping a user's information secret from itself.

The first problem of privacy is that of security from outside attack. Three phases in the life of private information concern us: security in preparation, in transport, and in storage. Preparation happens in the user's client (the software he uses to interact with a server). Transport means the ways and means by which the information is transferred between the client and the server. Security of storage deals with how the information is processed, stored, and protected on the vendor's computers.

In addition to security of client, transport, and storage, the user expects the vendor not to share his private information with other parties. The customary way of addressing this is for the vendor to make promises about how the user's private information will be used. This requires that the user trust the vendor's intentions, as well as his competence in securing transport and storage.

An alternative guarantee of privacy could be made if the user's private information is not accessible to the application vendor, even when stored on the vendor's own servers. This is commonly called "host proof", meaning that the information is withheld even from the server hosting it. The mechanism for achieving this is strong encryption, which must be applied to the private information before it is ever sent to the vendor's servers.

The problems of securing transport and storage are well known, much discussed, and mostly solved. These are areas where we have much room to maneuver. We can have control over the user's client software exactly to the extent that we write it ourselves. Almost full control can be achieved; the design and development of secure client software is not wild territory ... when creating native software. Native software development is a major investment, though, as it usually requires separate development for all supported platforms (e.g. Windows, Mac, Linux).

The difficulties in developing and marketing native applications have led to the recent explosion of web applications: software that runs largely in web browsers, driven by code downloaded from web sites and instantly available for use. It is now commonplace for technically unsophisticated users to create and store useful information with server-based applications, using an interface that lives in the ubiquitous web browser.

Web applications offer a tempting tradeoff: software development is significantly simplified, because most of the work has already been done for you. But from a security and privacy standpoint, most of the work has already been done for you!  Your choices are thus severely limited: A web browser is a platform which takes away most of the complexity of software development. The ability to write secure applications depends on the choices the browser does not make.

Can a browser-based application offer meaningful privacy? The answer depends on browsers, and on the platforms they offer.

## The Security of the Browser Platform

### Extensions

The most important thing to know about security in most browsers is this: extensions can completely compromise it. Browser extensions often have access to the full content of web pages, or to the cookies commonly used to store session or authentication data.

In Firefox particularly, every extension can access and manipulate all web pages and all stored data. Even the new HTML5 "local storage" (the contents of which are supposed to be restricted to the website or extension that created the data) is not secure under Firefox. Extensions may read and modify localStorage, cookies, website content, and even change the Javascript code that runs other applications. Far from being considered a problem, **this is the reason extensions exist**.

Google Chrome is far better in this respect; though it will still allow an extension to access arbitrary pages and data, it has fairly well thought out permissions, and it warns users what permissions an extension requests during the install process. It even protects HTML5 localStorage as the spec demands. But users are inured to warning dialogs, and will usually click through them without careful consideration.

How many Chrome extensions request dangerous permissions? Based on the top 100 most popular Chrome extensions (according to Chrome extension store):

* 98% of apps have some type of permission warning on install.
* 71% of apps have either the access "Your data on all websites" or "All data on your computer and the websites you visit" warnings.

[Chrome permissions warnings](http://code.google.com/chrome/extensions/permission_warnings.html#warnings)

Privacy in a web application is impossible to guarantee in Chrome or Firefox if the user has installed any third party extensions. Thus for the rest of this report, we assume that none are installed.

## Keeping information private with cryptography

If a user sends us his private information, naked and unprotected, he must trust that we will keep it safe. That we will refrain from reading and disseminating it. If we want to offer a service where the user does not have to trust our honesty and competence, the user must have a trustworthy way to encrypt his information before delivering it to us.

Users with the highest level of (arguably justifiable) paranoia will want to use their own encryption software, not anything provided by us. If we wish to read our users' private data, we can easily insert backdoors into our encryption routines. Assume for the sake of argument that our users do not trust us to protect unencrypted information, but do trust the software we provide for encrypting it. Encryption in native applications is, again, a well-traveled problem. This is not true in the browser, where there is a dearth of literature and surprising limitations.

Technical problems:

* Though browsers must have robust encryption implementations to communicate securely with web servers (HTTPS), no browser exposes these abilities to web applications.
* Thus cryptography in a web browser must be implemented in Javascript or in some embedded environment, such as Java or Flash.
* The nature and limitations of Javascript cause cryptography in the browser to be significantly slower than in native software.
* Browsers currently do not have a built-in ability to generate random data of the quality required for secure cryptography. This may change soon for Chrome, Safari, and other WebKit powered browsers.

Integrity issues:

* There are no standard, proven, broadly reviewed Javascript cryptography libs for the browser. In using other people's code, we may become vulnerable to intentional or unintentional flaws. Though this is true of all software in general, the consensus wisdom for cryptography is to distrust implementations that have not had broad testing and use.
* Implementing crypto correctly is difficult; if we do it ourselves, we are likely to get something wrong. There can be extremely bad consequences to getting something trivially wrong in cryptography, especially in public key encryption.
* Javascript makes it easy to alter application functions silently. If an attacker can inject Javascript code into an application after the cryptography library has loaded, he can replace encryption functions with anything he wants. This attack does require a vulnerability of some sort, but these vulnerabilities include such common practices as having extensions installed, or using a bookmarklet.
* In Firefox, any extension can overwrite the code from any other extension. Mozilla's answer to this is "Play nice." There appears to be a way to use XPCOM modules where the complelely open global namespace is fortified by the ability to instantiate a module from a specific file system location.
* Chrome does not suffer from this problem; no extension or website can modify an extension's Javascript code, and extensions cannot modify a website's Javascript.
Let us assume that we can overcome the integrity issues for some subset of applications. The next question is to what extent the technical problems pose obstructions.

## Cryptography overview

An application using cryptography must deal with two basic forms of encryption; something called a hash function; and the generation of reliably random values.

The two basic types of encryption are called symmetric and asymmetric. A hash function is used to produce what might be described as a verifiable, unforgeable cross-section of some piece of information. The random numbers generated for cryptographic protocols must have special properties to be considered secure.

### Symmetric encryption

Symmetric encryption is also called "secret key" encryption. The same key is used in both the encryption and decryption functions, so that key must be kept secret. It is reasonably fast and is therefore used when encrypting large amounts of information (meaning more than a short sentence). A critical problem with the use of secret key encryption is that its users must find some way to agree upon a secret key. These keys must be large and not easily guessable. Randomly generated keys are best.

### Asymmetric encryption

In asymmetric, also called "public key" encryption, the sender uses a public key that everyone knows to encrypt a message; there is an associated private key that can decrypt the message. Asymmetric encryption is extremely slow and vulnerable to attacks when used to encrypt large amounts of structured information. It is therefore commonly used only to encrypt short, random-looking messages.

Why is this useful? What kind of short, random-looking message would we possibly want to send someone?

How about 32 randomly selected characters?

The killer app for asymmetric cryptography is the secure transmission of a secret key, which the sender and receiver can then use to encrypt the rest of their conversation. Asymmetric cryptography is what makes any kind of secure encryption on the Internet actually possible. Without it, you would need to physically exchange secret keys with everyone you want to have private conversations with.

Asymmetric cryptography uses clever mathematical tricks such that you can encrypt something with a key you know, but cannot decrypt it. It's also ridiculously slow, and requires ridiculously large encryption keys. For example, in symmetric/secret key encryption, 128 bit long keys are still considered safe from brute forcing over the next 30 years. For the most commonly used asymmetric system, RSA, 4096 bit keys are now considered the minimum safe length.

For every additional bit of key length, you double the number of possible keys an attacker would have to try in a brute force attack. 129 bits can represent twice as many keys as 128. The difference between a 128 bit key and a 4096 bit key is the difference between 2 <sup>128</sup> and 2 <sup>4096</sup>. A 4096 bit key represented in decimal has about 1200 digits.

There's a tradeoff, of course, to having larger key sizes. Most obviously, it takes longer to generate longer keys. Secret keys used in symmetric crypto are straightforward, so a 256 bit key takes twice as long to generate as a 128 bit one. But the keys used in RSA public key crypto are generated from two extremely large prime numbers that must relate to each other in a certain way. Finding extremely large primes isn't exactly hard, but it is slow. This isn't crippling, because you don't need to generate public/private key pairs
very often.

What is more painful is that the mathematical calculations required to asymmetrically encrypt and decrypt a message become more labor intensive and take longer with longer keys. This is especially painful in a browser, because Javascript does not have native routines for working with Really Astoundingly Huge Integers. When you write such routines in Javascript, they are excruciatingly slow. If a browser application only uses asymmetric encryption at the beginning of a session, this is not exactly crippling, but it reaches well into the range of annoying, and possibly all the way up into frustrating.

### Hash functions

Briefly, how would you summarize Victor Hugo's *Les Miserable*? While you're thinking, here's how I would do it:

    f0bd171e7cece063e692531103712f374b7a9927fd45a7666df88ae13c7b7c9f

The first volume of Proust's *In Search of Lost Time*?

    f4a1cf5aa8abb44e2abb8c09f59358c842f69e7bd813b303cee3a06280b4160a

*War and Peace*?

    5d4114126159700cd6684b639bb07fb26355c29fff22b59c797fba42cb6920d0

Actually, I lied. That last summary is for Henry Fielding's *The History of Tom Jones, a Foundling*. The summary of *War and Peace* is:

    07f856faf2d2135a94c2c682be8fc42198fb25d3e16a8957eea3b792f58b9752

Now here's a hard one: how would you summarize the shortest verse in the New Testament?

    04f97d2714d935f78991ce849df2618d1af3c7cddf432be61257600de2bcf927

But how can you trust any of these values? I've already proven I'm a liar. I suppose you'll have to calculate those summaries yourself. The copy of *War and Peace* I used was the plain text from the Gutenberg project. Download that file, then generate its SHA256 hash value.

    openssl dgst -sha256 2600.txt.utf8

You can compare the output with the values I gave above to verify my story. Hashing operations are repeatable, and so the outputs are verifiable. The SHA256 hash for any given piece of text is **extremely likely** to be different from that of any other piece of text you can think of. For this reason, hashes are commonly used to identify texts, or to show when a message has been tampered with. If you change even a single letter in War and Peace, the resulting SHA256 digest is almost guaranteed to be different.

Of course, there are an infinite number of possible texts and a finite number of possible hash values, which means there are an infinite number of texts sharing any given hash value. In practice though, any good hash function distributes the short values among all possible texts in such a way that most source texts have a low chance of colliding. This alone is not sufficient for cryptographic purposes.

To be useful in cryptography, a hash function needs some additional strengths. It should be **extremely difficult** to figure out what the original message was. Further, it should be extremely difficult to generate any of the infinite number of messages that have the same hash value. It should be extremely difficult to modify the original text without changing the hash value. And (considering the function in general, not any particular message) it should be extremely difficult to find any two messages that have the same hash value. Creepy, eldritch attacks on crypto-systems become possible when they use a hash function for which you can find even random texts that collide.

Hashes, also called digests, are important to and widely used in cryptographic protocols. We won't go into that here; suffice it to say that computing the digest of some large chunk of data is a frequent occurrence.

### Encryption keys

Large, random values are used in many places in cryptography, but the most important is in generating keys (for both symmetric and asymmetric encryption). For symmetric, secret key encryption, you typically generate a random value of the right size and simply use it as the encryption and decryption key. Asymmetric key-pair generation is more involved, but it still requires generating large random numbers.

An encryption key must be large enough that an attacker can't simply try all the possible combinations. This is called a brute force attack. All modern encryption algorithms use at least 128 bit keys, which means an attacker would have to try 2<sup>128</sup> keys to be sure of decrypting any given message, but only about 2 <sup>64</sup> keys to have a very good chance. 2 <sup>64</sup> attempts at any procedure is considered to be computationally infeasible at this time.

128 bits is often represented as 16 ASCII characters, or as 32 hexadecimal characters (0-9,a-f). When a key must be humanly memorable, we can either force the human to choose a 16 character value, or we can let them pick something arbitrary and fit it to the key size. The simplest way to stretch out something shorter than needed is simply to pad it: fill in all the empty blanks with zeroes. Another way is to run the value through a hash function.

When you run a password through a hash function, it comes out looking impressively long and random. It would appear to be more secure. This is an illusion. The word "smurf" padded with 11 zeroes is exactly as secure a key as its hashed value (in hex): `8d98e1123092ea96213c438330fffb38f7974086`. Because hash functions are repeatable, they cannot add any randomness. A brute force attack can trivially try both "smurf" and its hash.

Thus encryption keys must use as much of the possible key-space as possible. Every character must count, and you do not want to restrict the set of characters used. A 16 letter key composed only of vowels is much less secure than a 16 letter key composed of consonants, vowels, and numbers. In the real world, the set of characters includes capital and lowercase letters, numbers, punctuation, as well as a bunch of values that are unprintable (as in, control characters, not obscenity). The sequence of characters should not be predictable.

In short, you want an encryption key to be as close to truly random as you can obtain.

### Random number generation

True randomness is probably impossible to obtain. Cryptography makes do with not-half-bad randomness collected from as many sources as possible: fluctuations in hard drive usage patterns, mouse movements, keystroke timings, inaudible noise recorded from a microphone jack when no microphone is connected. Even so, there's not enough not-half-bad randomness to go around, so we have to stretch it out, or reuse it, or use less of it at a time.

Reusing random values and using only small random values are both bad; which is worse depends on the context. Small random values are subject to brute force attacks. Repeated use of keys can allow some encryption algorithms to be broken in less than brute force time. Therefore, cryptographers have chosen to stretch out the randomness available to a system by using it as the input to Pseudo Random Number Generators.

A PRNG takes some initial input value and then produces a sequence of numbers that are very close to indistinguishable from number derived from true random noise. For any seed value, a given PRNG algorithm always produces the same sequence of numbers. This is very useful for situations where you need a bunch of random-looking numbers that you can repeat at will. With some additional stipulations (which we will not discuss here), you can use what randomness you can collect as the seed to a well-designed PRNG, effectively stretching out a limited amount of randomness.

Cryptographically secure PRNGs are a critical component of any crypto-system. Flaws in random number generation can have long term consequences on security. As an example, it was discovered recently that certain older versions of Debian Linux had a mistake in the code that seeded a PRNG, such that there were only 32,768 possible seed values. Such a small set of values is trivial to brute force. The consequence: any key-pairs generated on affected systems are effectively compromised, and all communications using those keys can be read.

> [http://digitaloffense.net/tools/debian-openssl/](http://digitaloffense.net/tools/debian-openssl/)
>
> All SSL and SSH keys generated on Debian-based systems (Ubuntu, Kubuntu, etc) between September 2006 and May 13th, 2008 may be affected. In the case of SSL keys, all generated certificates will be need to recreated and sent off to the Certificate Authority to sign. Any Certificate Authority keys generated on a Debian-based system will need be regenerated and revoked. All system administrators that allow users to access their servers with SSH and public key authentication need to audit those keys to see if any of them were created on a vulnerable system. Any tools that relied on OpenSSL's PRNG to secure the data they transferred may be vulnerable to an offline attack. Any SSH server that uses a host key generated by a flawed system is subject to traffic decryption and a man-in-the-middle attack would be invisible to the users. This flaw is ugly because even systems that do not use the Debian software need to be audited in case any key is being used that was created on a Debian system.


## Random number generation in the browser

Though browsers must have cryptographic routines to be able to speak SSL, none currently expose useful functions to application developers. Firefox has long had a `crypto.random()` function documented, but it is entirely unimplemented. An upcoming release of Chrome introduces an interface to the operating system's random number generator; this may also soon be in Safari. Mozilla developers appear to be waiting on permission from the WHATWG committee.

The only option in currently available browsers is to use a cryptographically secure PRNG in Javascript (or Flash, or Java). But there we run into the problem of a sufficiently random seed. 
Browsers do not have direct access to the kinds of low level system information most often used for collecting raw randomness (for good reason). The best sources of randomness available come from mouse movements and keypress timings. On some operating systems, keypress timing info available to browsers is not usefully random, so the only cross-platform way to gather something approximating randomness is with mouse movements. One research group estimates that it takes about a minute of mouse movements to collect the randomness needed to seed a PRNG. This is not user-friendly, to say the least.

## Performance of existing Javascript cryptography

Several cryptography libraries for Javascript exist, though no library I could find covers all possible needs. I ran a series of benchmarks of the best libraries on recent versions of Chrome, Safari, and Firefox. The performance of Javascript cryptography is not a showstopper, but it is decidedly disappointing. Doing cryptography in Javascript imposes heavy limitations on the amount of information an application can easily handle.

Key findings:

* Symmetric encryption and decryption (the kind we would use on feed text, e.g.) of 32KB takes about 20ms.
* It is not possible to generate a 4096 bit key-pair in a browser in the time the browser allows before declaring the script to be AWOL. 2048 bit pairs take more than 10 seconds.
* Hashing a 32KB piece of data takes about 10ms. We may want to obtain the hashes of large things like images, for which 32KB is a small size.

*2012 Note:* I no longer have the full benchmark results or code, and it is likely that the numbers have changed since this was written.  YMMV.



