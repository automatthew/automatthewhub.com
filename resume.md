# Matthew King  
[automatthew@gmail.com](automatthew@gmail.com)  
[http://github.com/automatthew](http://github.com/automatthew)

## Software Engineer

* **Specialties:** Distributed services, analytics, application privacy, load testing.
* **Languages:**  Ruby, Node.js, CoffeeScript, C, Java. HTML, CSS, Javascript, JQuery.  Conversant with Erlang, Forth. 
* **Tools:**  Redis, PostgreSQL, MySQL. HAProxy, Mongrel2, 0mq. Lucene, Solr, MongoDB. Git, Subversion, Perforce. 

## Experience

### Senior Software Engineer @ spire.io (originally Border Stylo): 2010 - May 2012

*Feb - Sep 2010:* Architect and lead developer for a Ruby+Rack-based ReST API that replaced an existing RPC backend for our link-sharing browser extension.  Developed an HTTP push service, used by the extension for real-time event notifications.  Developed load testing tools for assessing capacity of the ReST backend and the HTTP push service.  Assisted with deploys and other system administration tasks during a period when the company lacked dedicated operations engineers.

*Oct 2010 - Apr 2011:* Led assorted R&D projects: host proof applications, browser extension security, browser-based crypto, web content curation (i.e. identifying content at a level lower than actual pages:  images, embedded videos, blog posts, facebook users).

*May - Aug 2011:* Assisted in development of a mobile app that used our existing backend. Developed analytics tools for assessing the extension and mobile app usage.

*Sep - Dec 2011:*  R&D for a new distributed backend, to support a ReST API that would itself become our product.  Developed a high-concurrency load testing tool to test alternative architectures.

*Jan - Jun 2012:*  Implementation of the "Backend as a Service" backend.  Led a team of developers in the further implementation necessary for public release.  Designed the Ruby reference client for the API.  Developed Redis-based analytics tools.

### Research Engineer @ AT&T Interactive Research and Development: 2009-2010

Development, documentation, and release management of a Ruby web application framework, and a stable of related open source projects (AutoCode, Functor, Filebase).

Ruby and Java interfaces for AT&T's Finite State Machine library (using ruby-ffi and JNA).  Prototyped phrase recognition routines using the Ruby interface, then ported the algorithms to C. Integrated the resulting library into Lucene and Solr search engines.

JRuby wrapper for Lucene to facilitate prototyping "local search" engines, e.g. the Have2P and Speak4It iPhone apps.

Solr and Lucene extensions to make possible use of "Learning to Rank" IR algorithms.

### IT Director @ Thelese Management: 2005-2008

Networks, email, LDAP, web development using Ruby on Rails and Merb.

### Security Analyst & Support Engineer @ CynergisTek Inc.: 2004-2005
Vulnerability scanning, war dialing.  Maintained network, web, email, version control. Scripted a GUI tool for integrating version control into the Mac OS X Finder.

### Enterprise Support @ Apple Computer: 2001-2004

Technical support for enterprise products:  Mac OS X Server, Xserve, Xserve RAID, XSan.


\newpage



