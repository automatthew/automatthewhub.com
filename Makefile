all: resume.pdf resume.html

%.html: %.md
	pandoc -t html -o $@ $< -c resume.css

%.pdf:  %.md 
	pandoc \
		-H resume-header.tex \
		-f markdown \
		-o $@ $<


%.tex:  %.md found-template.tex
	pandoc -H resume-header.tex -f markdown -o $@ $<

clean:
	rm -f resume.pdf
